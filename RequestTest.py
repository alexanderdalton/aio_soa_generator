import requests, xmltodict, aiohttp, asyncio
import datetime as dt
from functools import wraps
from asyncio.proactor_events import _ProactorBasePipeTransport

# url = "http://172.0.0.214:8080/admin/ws/ContractService?wsdl"
# user = 'AIO Support tools'
# country = 'AU'
# headers = {'Content-Type': 'text/xml;charset=UTF-8'}

# data = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://www.sving.com/schemas/common" xmlns:con="http://www.sving.com/schemas/contract">
#     <soapenv:Header>
#         <com:sfRequest country="%s" user="%s"/>
#     </soapenv:Header>
#     <soapenv:Body>
#         <con:outstandingAmountRequest id="%s">
#             <con:date>%s</con:date>
#         </con:outstandingAmountRequest>
#     </soapenv:Body>
# </soapenv:Envelope>""" % (country, user, contract_ID, date)

# response = requests.post(url, data, headers)
# response_dict = xmltodict.parse(response.content)
# print(response_dict['soap:Envelope']['soap:Body']['outstandingAmountResponse']['principal'])
# # output = str(response.content, 'utf-8')

# # faultstring = re.search('<faultstring>(.*)</faultstring>', output)
# # if faultstring is not None:
# #     totalAmount = 'ERROR: ' + faultstring.group(1)[:80]+'...' if len(faultstring.group(1))>80 else faultstring.group(1)
# #     principal = interest = lateInterest = fees = normalInvoiceables = lateInvoiceables = '?'

# print(output)

def silence_event_loop_closed(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except RuntimeError as e:
            if str(e) != 'Event loop is closed':
                raise
    return wrapper

_ProactorBasePipeTransport.__del__ = silence_event_loop_closed(_ProactorBasePipeTransport.__del__)

async def doGet(session, url):
    async with session.get(url) as response:
        resp = await response.json()
        principal_outstanding = (resp['contractBalanceEntries'][0]['balance']['PRINCIPAL']['outstanding'])/100
        principal_overdue = (resp['contractBalanceEntries'][0]['balance']['PRINCIPAL']['amountOverdue'])/100
        interest_outstanding = (resp['contractBalanceEntries'][0]['balance']['INTEREST']['outstanding'])/100
        interest_overdue = (resp['contractBalanceEntries'][0]['balance']['INTEREST']['amountOverdue'])/100
        print('Principal outstanding: '+str(principal_outstanding))
        print('Principal overdue: '+str(principal_overdue))
        print('Interest outstanding: '+str(interest_outstanding))
        print('Interest overdue: '+str(interest_overdue))
        return resp

async def yieldDates(date_list):
    for date in date_list:
        yield date

async def main():
    date_list = ['2019-08-02', '2019-08-03', '2019-08-04', '2019-08-05', '2019-08-06', '2019-08-07', '2019-08-08', '2019-08-09']
    today = dt.date.today().strftime("%Y-%m-%d")

    async def getPrincipalAtDates(date_list):

        async with aiohttp.ClientSession() as session:
            tasks = []

            async for date in yieldDates(date_list):
                url = 'https://balance-cache-int.prod.ipfdigital.io/v1/balance/contract/4402193?endDate={}&freezeDate={}'.format(date, today)
                tasks.append(doGet(session, url))
            
            results = await asyncio.gather(*tasks)

        return results

    results = await getPrincipalAtDates(date_list)

if __name__ == "__main__":
    asyncio.run(main())