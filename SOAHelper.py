import os, aiohttp, asyncio
import pandas as pd
import datetime as dt
from functools import wraps
from asyncio.proactor_events import _ProactorBasePipeTransport

class SOAHelper:

    db_con = None
    con_id = 0
    app_status = None
    current_dir = os.path.dirname(__file__) 

    def __init__(self, passed_db_con, passed_con_id):
        self.db_con = passed_db_con
        self.con_id = passed_con_id
        # silences exception produced by asyncio when gathering our get requests in getBalancesAtDates
        _ProactorBasePipeTransport.__del__ = self.silence_event_loop_closed(_ProactorBasePipeTransport.__del__)

    def silence_event_loop_closed(self, func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            try:
                return func(self, *args, **kwargs)
            except RuntimeError as e:
                if str(e) != 'Event loop is closed':
                    raise
        return wrapper

    def getInvoices(self):
        invoices_q = f"""
                        select
                        c.ID as cust_id,
                        con.ID as con_id,
                        con.stateType as con_state,
                        i.ID as inv_id,
                        i.due_date,
                        i.amount/100 as inv_amount,
                        i.stateType as inv_state
                        from Customer c

                        inner join Contract con on (c.ID = con.customer_ID)
                        inner join Invoice i on (con.ID = i.contract_ID and i.amount != 0)

                        where c.country = 'AU'
                        and con.ID = {self.con_id}

                        order by i.ID asc
                        """
        
        return pd.read_sql_query(invoices_q, self.db_con)

    def getPayments(self):
        payments_q = f"""
                        select
                        c.ID as cust_id,
                        con.ID as con_id,
                        con.stateType as con_state,
                        ai.ID as ai_id,
                        p.ID as payment_id,
                        p.amount/-100 as payment_amt,
                        p.valueDate as payment_date,
                        p.extraInfo
                        from Customer c

                        inner join AllocationInstructions ai on (c.ID = ai.customer_ID)
                        inner join Payment p on (ai.payment_ID = p.ID and (p.stateType != 'DELETED' or p.stateType is null))
                        inner join Allocation a on (p.ID = a.payment_id)
                        inner join CashFlow cf on (a.cashFlow_ID = cf.ID)
                        inner join Contract con on (cf.contract_ID = con.ID)
                        left join Payment pp on (p.ID = pp.ID and p.valueDate = date(con.closed) and con.stateType = 'SOLD')
                        left join Payment ppp on (p.ID = ppp.ID and p.valueDate >= date(con.closed) and con.stateType = 'CONTRACT_CHANGE')

                        where c.country = 'AU'
                        and con.ID = {self.con_id}
                        and pp.ID is null
                        and ppp.ID is null

                        group by p.ID
                        order by p.valueDate asc
                        """

        return pd.read_sql_query(payments_q, self.db_con)

    def getRefunds(self):
        refunds_q = f"""
                        select
                        c.ID as cust_id,
                        con.ID as con_id,
                        con.stateType as con_state,
                        ai.ID as ai_id,
                        p.ID as payment_id,
                        p.amount/-100 as payment_amt,
                        p.paymentDate,
                        p.extraInfo,
                        date(cf.created) as refund_date,
                        a.amount/-100 as refund_amt
                        from Customer c

                        inner join AllocationInstructions ai on (c.ID = ai.customer_ID)
                        inner join Payment p on (ai.payment_ID = p.ID)
                        left join Allocation a on (p.ID = a.payment_id)
                        left join CashFlow cf on (a.cashFlow_ID = cf.ID)
                        left join Contract con on (cf.contract_ID = con.ID)

                        where c.country = 'AU'
                        and con.ID = {self.con_id}
                        and cf.type = 'RETURN'

                        order by p.paymentDate asc
                        """

        return pd.read_sql_query(refunds_q, self.db_con)

    def getDraws(self):
        draw_q = f"""
                    select
                    c.ID as cust_id,
                    con.ID as con_id,
                    draw.ID as draw_id,
                    date(date_add(cf.created, interval 10 hour)) as date,
                    draw.amount/100 as amount
                    from Customer c

                    inner join Contract con on (c.ID = con.customer_ID)
                    inner join NewDrawdownCase draw on (con.ID = draw.contract_ID)
                    inner join CashFlow cf on (draw.principalCashFlow_ID = cf.ID)
                    inner join CreditApplication a on (con.creditApplication_ID = a.ID)
                    inner join Product p on a.product_ID = p.ID
                    left join (select min(NewDrawdownCase.ID) as id from Customer
                    inner join Contract on Customer.ID = Contract.customer_ID
                    inner join NewDrawdownCase on Contract.ID = NewDrawdownCase.contract_ID 
                    where Customer.country = 'AU'
                    and Contract.ID = {self.con_id}) fdraw on (draw.ID = fdraw.id)
                    left join Contract pcon on (c.ID = pcon.customer_ID and pcon.contracts_ORDER = con.contracts_ORDER - 1 and pcon.stateType = 'CONTRACT_CHANGE' and p.type = 'CREDIT_LINE')

                    where c.country = 'AU'
                    and con.ID = {self.con_id}
                    and (fdraw.ID is null or pcon.ID is not null)

                    order by draw.ID asc
                    """

        return pd.read_sql_query(draw_q, self.db_con)

    def getLateFees(self):
        late_fee_q = f"""
                        select
                        c.ID as cust_id,
                        con.ID as con_id,
                        con.stateType as con_state,
                        cf.ID as cf_id,
                        date(cf.created) as cf_created,
                        cf_fees.fees
                        from Customer c

                        inner join Contract con on (c.ID = con.customer_ID)
                        inner join CashFlow cf on (con.ID = cf.contract_ID)
                        left join Allocation a on (cf.ID = a.cashFlow_ID)

                        left join (select date(CashFlow.created) as cf_date, sum(CashFlow.amount)/-100 as fees from Customer
                        inner join Contract on Customer.ID = Contract.customer_ID
                        inner join CashFlow on Contract.ID = CashFlow.contract_ID
                        where Customer.country = 'AU'
                        and Contract.ID = {self.con_id}
                        and (CashFlow.DTYPE = 'FeeCashFlow' and CashFlow.type = 'LATE_FEE')
                        and CashFlow.amount != 0.00
                        group by date(CashFlow.created)) cf_fees on (date(cf.created) = cf_fees.cf_date)

                        where c.country = 'AU'
                        and con.ID = {self.con_id}
                        and cf.amount != 0.00
                        and cf.DTYPE = 'FeeCashFlow' and cf.type = 'LATE_FEE'

                        group by cf.created
                        order by cf.ID asc
                        """
        
        return pd.read_sql_query(late_fee_q, self.db_con)

    # DEFINITIONS:
    # Upsold - contract has been upsold (contract state is CONTRACT_CHANGE)
    # Upgraded - contract has been upgraded (contract state is CONTRACT_CHANGE)
    # Upsell - previous contract was upsold to this contract
    # Upgrade - previous contract was upgraded to this contract
    def getContractDetails(self):
        con_details_q = f"""
                                select
                                c.ID as cust_id,
                                c.firstName as first_name,
                                c.lastName as last_name,
                                con.ID as con_id,
                                date(date_add(con.created, interval 10 hour)) as created,
                                draw.ID as draw_id,
                                date(date_add(cf.created, interval 10 hour)) as date,
                                p.principal/100 as principal,
                                a.firstDrawAmount/100 as first_draw,
                                p.establishmentFee/100 as est_fee,
                                p.type,
                                p.interest,
                                case when con.stateType is null then 'OPEN'
                                else con.stateType end as state,
                                case when con.closed is not null then date(date_add(con.closed, interval 10 hour))
                                when con.closed is null then curdate()
                                end as con_closed,
                                case when con.stateType = 'CONTRACT_CHANGE' and p.type = 'INSTALLMENT' then 'yes'
                                else 'no' end as upsold,
                                case when con.stateType = 'CONTRACT_CHANGE' and p.type = 'CREDIT_LINE' then 'yes'
                                else 'no' end as upgraded,
                                case when upgraded_cl.ID is not null then 'yes'
                                else 'no' end as upgrade,
                                case when upsold_il.ID is not null then 'yes'
                                else 'no' end as upsell
                                from Customer c

                                inner join Contract con on (c.ID = con.customer_ID)
                                inner join NewDrawdownCase draw on (con.ID = draw.contract_ID)
                                inner join CashFlow cf on (draw.principalCashFlow_ID = cf.ID)
                                left join (select min(NewDrawdownCase.ID) as id from Customer
                                inner join Contract on Customer.ID = Contract.customer_ID
                                inner join NewDrawdownCase on Contract.ID = NewDrawdownCase.contract_ID 
                                where Customer.country = 'AU'
                                and Contract.ID = {self.con_id}
                                ) fdraw on (draw.ID = fdraw.id)
                                inner join CreditApplication a on (con.creditApplication_ID = a.ID)
                                inner join Product p on (a.product_ID = p.ID)
                                left join Contract upgraded_cl on (con.customer_ID = upgraded_cl.customer_ID and upgraded_cl.contracts_ORDER = con.contracts_ORDER-1 and upgraded_cl.stateType = 'CONTRACT_CHANGE' and p.type = 'CREDIT_LINE' and con.hierarchy_ID = upgraded_cl.hierarchy_ID)
                                left join Contract upsold_il on (con.customer_ID = upsold_il.customer_ID and upsold_il.contracts_ORDER = con.contracts_ORDER-1 and p.type = 'INSTALLMENT' and con.hierarchy_ID = upsold_il.hierarchy_ID)

                                where c.country = 'AU'
                                and con.ID = {self.con_id}
                                and fdraw.ID is not null

                                order by draw.ID asc
                                """

        return pd.read_sql_query(con_details_q, self.db_con)

    def getListOfPreviousContracts(self):
        q_con_list =    f"""
                        select
                        c.ID as cust_id,
                        con.ID as con_id,
                        pcon.ID as prev_con
                        from Customer c

                        inner join Contract con on (c.ID = con.customer_ID)
                        inner join CreditApplication a on (con.creditApplication_ID = a.ID)
                        inner join Product p on (a.product_ID = p.ID)
                        left join Contract pcon on (c.ID = pcon.customer_ID and pcon.contracts_ORDER < con.contracts_ORDER and pcon.stateType = 'CONTRACT_CHANGE')
                        left join CreditApplication aa on (pcon.creditApplication_ID = aa.ID)
                        left join Product pp on (aa.product_ID = pp.ID)

                        where c.country = 'AU'
                        and con.ID = {self.con_id}
                        and pp.type = 'CREDIT_LINE'
                        """

        con_list = pd.read_sql_query(q_con_list, self.db_con)
        prev_con_list = [int(self.con_id)]

        for con in con_list['prev_con']:
            prev_con_list.append(con)

        return prev_con_list

    async def getBalancesAtDates(self, sdate, upgraded):
        async with aiohttp.ClientSession() as session:
            tasks = []
            date_list = self.generateDates(sdate)

            if upgraded == 'yes':
                contracts = self.getListOfPreviousContracts()
            else:
                contracts = [self.con_id]

            async for date in self.yieldDates(date_list):
                tasks.append(self.doGet(date, session, contracts))
            
            results = await asyncio.gather(*tasks)

        return results

    def generateDates(self, sdate):
        today = dt.date.today()
        days_between_dts = (today - sdate).days
        i = 0

        date_list = [sdate.strftime('%Y-%m-%d')]
        date_to_add = sdate
        while i < days_between_dts:
            date_to_add = date_to_add + dt.timedelta(days=1)
            date_list.append(date_to_add.strftime('%Y-%m-%d'))
            i = i+1

        return date_list

    async def yieldDates(self, date_list):
        for date in date_list:
            yield date

    # Web service freeze date should always be tomorrow so we're able to pull the current date too (freeze date has to be in the future)
    # Web service uses pulls info up to but not included the specified date (<)
    # We feed this function a list of contracts. It's always fed a singular contract ID unless it's an upgrade, then we feed self.con_id and all previous CL contracts
    async def doGet(self, date, session, contracts):
        tomorrow = (dt.date.today() + dt.timedelta(days=1)).strftime('%Y-%m-%d')
        date_to_get = (dt.datetime.strptime(date, '%Y-%m-%d') + dt.timedelta(days=1)).strftime('%Y-%m-%d')
        url = 'https://balance-cache-int.prod.ipfdigital.io/v1/balance/contract/{}?endDate={}&freezeDate={}'.format(self.con_id, date_to_get, tomorrow)

        async with session.get(url) as response:
        
            resp = await response.json()

            principal_outstanding = 0
            try:
                principal_outstanding = (resp['contractBalanceEntries'][0]['balance']['PRINCIPAL']['outstanding'])/100
            except KeyError:
                principal_outstanding = principal_outstanding

            principal_overdue = 0
            try:
                principal_overdue = (resp['contractBalanceEntries'][0]['balance']['PRINCIPAL']['amountOverdue'])/100
            except KeyError:
                principal_overdue = principal_overdue

            principal_generated = 0
            try:
                principal_generated = (resp['contractBalanceEntries'][0]['balance']['PRINCIPAL']['amountGenerated'])/100
            except KeyError:
                principal_generated = principal_generated

            principal_paid = 0
            try:
                principal_paid = (resp['contractBalanceEntries'][0]['balance']['PRINCIPAL']['amountPaid'])/100
            except KeyError:
                principal_paid = principal_paid

            interest_outstanding = 0
            for contract in resp['contractBalanceEntries']:
                if contract['contractId'] in contracts:
                    try:
                        interest_outstanding = interest_outstanding + (contract['balance']['INTEREST']['outstanding'])/100
                    except KeyError:
                        interest_outstanding = interest_outstanding

            interest_overdue = 0
            for contract in resp['contractBalanceEntries']:
                if contract['contractId'] in contracts:
                    try:
                        interest_overdue = interest_overdue + (contract['balance']['INTEREST']['amountOverdue'])/100
                    except KeyError:
                        interest_overdue = interest_overdue
            
            interest_generated = 0
            for contract in resp['contractBalanceEntries']:
                if contract['contractId'] in contracts:
                    try:
                        interest_generated = interest_generated + (contract['balance']['INTEREST']['amountGenerated'])/100
                    except KeyError:
                        interest_generated = interest_generated

            interest_paid = 0
            for contract in resp['contractBalanceEntries']:
                if contract['contractId'] in contracts:
                    try:
                        interest_paid = interest_paid + (contract['balance']['INTEREST']['amountPaid'])/100
                    except KeyError:
                        interest_paid = interest_paid

            interest_credited = 0
            for contract in resp['contractBalanceEntries']:
                if contract['contractId'] in contracts:
                    try:
                        interest_credited = interest_credited + (contract['balance']['INTEREST']['amountCredited'])/100
                    except KeyError:
                        interest_credited = interest_credited

            fee_outstanding = 0
            for contract in resp['contractBalanceEntries']:
                if contract['contractId'] in contracts:
                    try:
                        fee_outstanding = fee_outstanding + (contract['balance']['PROVISIONING_FEE']['outstanding'])/100
                    except KeyError:
                        fee_outstanding = fee_outstanding

            fee_overdue = 0
            for contract in resp['contractBalanceEntries']:
                if contract['contractId'] in contracts:
                    try:
                        fee_overdue = fee_overdue + (contract['balance']['PROVISIONING_FEE']['amountOverdue'])/100
                    except KeyError:
                        fee_overdue = fee_overdue
            
            fee_generated = 0
            for contract in resp['contractBalanceEntries']:
                if contract['contractId'] in contracts:
                    try:
                        fee_generated = fee_generated + (contract['balance']['PROVISIONING_FEE']['amountGenerated'])/100
                    except KeyError:
                        fee_generated = fee_generated

            
            fee_paid = 0
            for contract in resp['contractBalanceEntries']:
                if contract['contractId'] in contracts:
                    try:
                        fee_paid = fee_paid + (contract['balance']['PROVISIONING_FEE']['amountPaid'])/100
                    except KeyError:
                        fee_paid = fee_paid

            fee_credited = 0
            for contract in resp['contractBalanceEntries']:
                if contract['contractId'] in contracts:
                    try:
                        fee_credited = fee_credited + (contract['balance']['PROVISIONING_FEE']['amountCredited'])/100
                    except KeyError:
                        fee_credited = fee_credited

            late_generated = 0
            for contract in resp['contractBalanceEntries']:
                if contract['contractId'] in contracts:
                    try:
                        late_generated = late_generated + (contract['balance']['LATE_FEE']['amountGenerated'])/100
                    except KeyError:
                        late_generated = late_generated

            late_credited = 0
            for contract in resp['contractBalanceEntries']:
                if contract['contractId'] in contracts:
                    try:
                        late_credited = late_credited + (contract['balance']['LATE_FEE']['amountCredited'])/100
                    except KeyError:
                        late_credited = late_credited     
        
        return {'date':date,
                'principal_outstanding':principal_outstanding,
                'principal_overdue':principal_overdue,
                'principal_generated':principal_generated, 
                'principal_paid':principal_paid,
                'interest_outstanding':interest_outstanding,
                'interest_overdue':interest_overdue,
                'interest_generated':interest_generated,
                'interest_paid':interest_paid,
                'interest_credited':interest_credited,
                'fee_outstanding':fee_outstanding,
                'fee_overdue':fee_overdue,
                'fee_generated':fee_generated,
                'fee_credited':fee_credited,
                'fee_paid':fee_paid,
                'late_generated':late_generated,
                'late_credited':late_credited
                }