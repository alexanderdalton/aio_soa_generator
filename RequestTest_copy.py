import requests, xmltodict, aiohttp, asyncio
import datetime as dt

async def main():
    async with aiohttp.ClientSession() as session:
        async with session.get('https://balance-cache-int.prod.ipfdigital.io/v1/balance/contract/4402193?endDate=2021-01-01&freezeDate=2021-06-07') as resp:
            print(await resp.content)

if __name__ == "__main__":
    asyncio.run(main())