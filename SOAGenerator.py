import os, asyncio
import pandas as pd
import datetime as dt
from SOAHelper import SOAHelper
from openpyxl import load_workbook
from Authentication import ConnectToDB

class SOAGenerator:

    con_id = 0
    today = (dt.date.today()).strftime("%Y-%m-%d")

    def __init__(self, con_id, passed_db_con):
        self.con_id = con_id

        self.balance = 0
        self.interest_accrued = 0
        self.interest_charged = 0
        self.interest_credited = 0
        self.fee_charged = 0
        self.fee_credited = 0
        self.late_credited = 0
        self.row = 2
        self.sheet = ""
        if os.path.dirname(__file__) == '':
            self.current_dir = "."
        else:
            self.current_dir = os.path.dirname(__file__)
        self.wb = None

        #a bit of setup
        self.db_con = passed_db_con
        self.helper = SOAHelper(self.db_con, con_id)

        # pull relevant data we need
        self.invoices = self.helper.getInvoices()
        self.payments = self.helper.getPayments()
        self.refunds = self.helper.getRefunds()
        self.late_fees = self.helper.getLateFees()
        self.draws = self.helper.getDraws()
        self.contract_details = self.helper.getContractDetails()
        self.balances = asyncio.run(self.helper.getBalancesAtDates(self.contract_details['date'][0], self.contract_details['upgrade'][0]))

        #write info to our sheet
        self.setupSheet()
        self.doAIOLoop()
        self.addFinalBalance()

        #save info to file
        self.saveFile()

    def addRow(self, col_a = "", col_b = "", debit = "", credit = "", balance_override = None):

        if balance_override is not None:
            self.balance = balance_override
        elif debit != "":
            self.balance = self.balance - float(debit)
        elif credit != "":
            self.balance = self.balance + float(credit)

        self.sheet['A'+str(self.row)] = col_a
        self.sheet['B'+str(self.row)] = col_b
        self.sheet['C'+str(self.row)] = debit
        self.sheet['D'+str(self.row)] = credit
        self.sheet['E'+str(self.row)] = self.balance
        self.row += 1

    def setupSheet(self):
        # load template and set sheet to Sheet1
        self.wb = load_workbook(self.current_dir + '/soa_generator_template.xlsx')
        self.sheet = self.wb['Sheet1']

        # populate principal and est. fee at the beginning of the SOA (cells D2, D3)
        if self.contract_details['upgrade'][0] == 'yes':
            outstanding_balance = self.balances[0]['principal_outstanding'] + self.balances[0]['interest_outstanding']
            self.interest_charged = self.balances[0]['interest_generated']
            self.addRow(self.contract_details['date'][0], 'Outstanding amount', '', outstanding_balance)
        elif self.contract_details['type'][0] == 'INSTALLMENT':
            self.addRow(self.contract_details['date'][0], 'Loan Principal','',self.contract_details['principal'][0])
            self.addRow(self.contract_details['date'][0], 'Establishment Fee','',self.contract_details['est_fee'][0])
        elif self.contract_details['type'][0] == 'CREDIT_LINE':
            self.addRow(self.contract_details['date'][0], 'First Draw','',self.contract_details['first_draw'][0])

    def doAIOLoop(self):

        for day in self.balances:
            date = dt.datetime.strptime(day['date'], '%Y-%m-%d').date()

            # AIO creates credit memos on the sold date which aren't relevant to the SOA. We want to skip these, add interest accrued to the sold date and terminate
            if (self.contract_details['state'][0] == 'SOLD' and date == self.contract_details['con_closed'][0]):
                self.addSoldInterest(date, day['interest_generated'])
                break

            # Find rows with the same working date in all of our dataframes
            draws = self.draws.loc[self.draws['date'] == date]
            payments = self.payments.loc[self.payments['payment_date'] == date]
            late_fees = self.late_fees.loc[self.late_fees['cf_created'] == date]
            invoices = self.invoices.loc[self.invoices['due_date'] == date]
            refunds = self.refunds.loc[self.refunds['refund_date'] == date]

            # If the contract is already closed don't accrue any interest. Continue the loop as there may be further payments/refunds/credits
            # If the contract was upsold we should not accrue interest on the contract closed date.
            ##########
            # Any interest accrued after the customer has applied for an upsell is credit memoed once the app is approved. AIO sets the contract closed date to the
            # upsell application created date upon approval, therefore we can still use the contract close date but accrue interest up to the day before instead
            ##########
            # We should accrue interest where the contract is not a SACC. SACC's get fees
            if ((date <= self.contract_details['con_closed'][0] and self.contract_details['upsold'][0] == 'no')
            or (date < self.contract_details['con_closed'][0]) and self.contract_details['upsold'][0] == 'yes'):
                if not (self.contract_details['type'][0] == 'INSTALLMENT' and self.contract_details['principal'][0] <= 2000):
                    self.interest_accrued = day['interest_generated'] - self.interest_charged

            # Add each draw for the working date
            for draw in draws['amount']:
                row_data = {'date':date, 'description':'Draw', 'DR':'', 'CR':draw , 'balance':None}
                self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])
            
            # If an invoice is due today and the contract is a CL or an IL-MACC, add interest to the SOA and set accrued interest back to 0
            if len(invoices['due_date']) > 0:
                if ((self.contract_details['type'][0] == 'CREDIT_LINE')
                or (self.contract_details['type'][0] == 'INSTALLMENT' and self.contract_details['principal'][0] > 2000)):
                    self.addInterest(date)

            # If any account servicing fees have been generated but not charged add them to the SOA. Set our fee_charged to reflect web-service so we can continue checking
            if day['fee_generated'] > self.fee_charged:
                account_servicing_fee = day['fee_generated'] - self.fee_charged
                row_data = {'date':date, 'description':'Account Servicing Fee', 'DR':'', 'CR':account_servicing_fee, 'balance':None}
                self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])
                self.fee_charged = day['fee_generated']

            # Add each late fee for the working date
            for fee in late_fees['fees']:
                row_data = {'date':date, 'description':'Late Fee', 'DR':'', 'CR':fee , 'balance':None}
                self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])

            # If any late fee credit memo have but generated but not credited add them to the SOA. Set our late_credited to reflect web-service so we can continue checking
            if day['late_credited'] > self.late_credited:
                credit_memo = day['late_credited'] - self.late_credited
                row_data = {'date':date, 'description':'Credit Memo - Late Fee', 'DR':credit_memo, 'CR':'' , 'balance':None}
                self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])
                self.late_credited = day['late_credited']
            
            # If any interest or account-service fee's credit memos have been generated but not credited then add them to the SOA. Set our credits to reflect web-service
            # Upsold loans have credit memos applied on the contract close date which pay out the loan and move the balance to the next contract, therefore we should not apply these credits
            # SACC loans that are paid out sometimes have credit memos generated after contract close date, therefore we should differentiate between upsells and normal loans
            if (day['interest_credited'] > self.interest_credited and date < self.contract_details['con_closed'][0]):
                self.addCreditMemos(date, day['interest_credited'], day['fee_credited'])
            elif (day['fee_credited'] > self.fee_credited and date < self.contract_details['con_closed'][0] and self.contract_details['upsold'][0] == 'yes'):
                self.addCreditMemos(date, day['interest_credited'], day['fee_credited'])
            elif (day['fee_credited'] > self.fee_credited and self.contract_details['upsold'][0] == 'no'):
                self.addCreditMemos(date, day['interest_credited'], day['fee_credited'])

            # Add each payment for the working date
            for payment in payments['payment_amt']:
                row_data = {'date':date, 'description':'Payment Received', 'DR':payment, 'CR':'', 'balance':None}
                self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])
            
            # Add each refund for the working date
            for refund in refunds['refund_amt']:
                row_data = {'date':date, 'description':'Refund', 'DR':'', 'CR':refund, 'balance':None}
                self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])
            
        # Once the loop has finished add any accrued interest to our balance in preperation for adding our final balance
        self.balance = self.balance + self.interest_accrued

    def addInterest(self, date):
        if self.interest_accrued > 0:
            row_data = {'date':date, 'description':'Interest Charged', 'DR':'', 'CR':self.interest_accrued, 'balance':None}
            self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])
            self.interest_charged = self.interest_charged + self.interest_accrued
            self.interest_accrued = 0

    def addCreditMemos(self, date, ws_interest_credited, ws_fee_credited):
        # If it's an IL then check web-service fees
        if self.contract_details['type'][0] == 'INSTALLMENT' and self.contract_details['first_draw'][0] <= 2000:
            credit_memo = ws_fee_credited - self.fee_credited
            row_data = {'date':date, 'description':'Credit Memo - Account Servicing Fee', 'DR':credit_memo, 'CR':'', 'balance':None}
            self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])
            self.fee_credited = ws_fee_credited
        # otherwise it's a MACC or CL so check web-service interest
        else:
            credit_memo = ws_interest_credited - self.interest_credited
            row_data = {'date':date, 'description':'Credit Memo - Interest', 'DR':credit_memo, 'CR':'', 'balance':None}
            self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])
            self.interest_credited = ws_interest_credited

    def addSoldInterest(self, sold_date, ws_interest_generated):
        # If the contract isn't a SACC then add any accrued interest at the sale date
        if not (self.contract_details['type'][0] == 'INSTALLMENT' and self.contract_details['first_draw'][0] <= 2000):
            self.interest_accrued = ws_interest_generated - self.interest_charged
            row_data = {'date':sold_date, 'description':'Interest Accrued', 'DR':'', 'CR':self.interest_accrued, 'balance':None}
            self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])
            self.interest_charged = self.interest_charged + self.interest_accrued
            self.interest_accrued = 0

    def addFinalBalance(self):
        if self.contract_details['state'][0] == 'SOLD':
            row_data = {'date':self.contract_details['con_closed'][0], 'description':'Debt Assigned', 'DR':'', 'CR':'', 'balance':None}
            self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])
        
        elif (self.contract_details['state'][0] == 'CLOSED' or self.contract_details['state'][0] == 'PAID'):
            row_data = {'date':self.contract_details['con_closed'][0], 'description':'Closing Balance', 'DR':'', 'CR':'', 'balance':None}
            self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])
        
        # assumption - balance remaining at this stage is the upold payout figure
        elif (self.contract_details['upsold'][0] == 'yes'):
            upsold_credit = self.balance
            row_data = {'date':self.contract_details['con_closed'][0], 'description':'Top Up Payout', 'DR':upsold_credit, 'CR':'', 'balance':None}
            self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])

        elif (self.contract_details['upgraded'][0] == 'yes'):
            row_data = {'date':self.contract_details['con_closed'][0], 'description':'Closing Balance', 'DR':'', 'CR':'', 'balance':None}
            self.addRow(row_data['date'], row_data['description'], row_data['DR'], row_data['CR'], row_data['balance'])

        elif self.contract_details['state'][0] == 'OPEN':
            self.addRow(self.today, 'Outstanding Balance', '', '', balance_override=None)

    def saveFile(self):
        customer_name = self.contract_details['first_name'][0] + ' ' + self.contract_details['last_name'][0]
        self.wb.save(self.current_dir+'/generated_files/soa_'+str(self.con_id)+'_'+customer_name+'.xlsx')
        print('Saved soa_'+str(self.con_id)+'_'+customer_name+'.xlsx')


user_input = input("\nProvide the contract ID or a list of contract ID's seperated by commas. Press 0 to exit.\nContract ID: ")
if user_input == '0':
    exit

con_id_list = user_input.split(',')

db_con = ConnectToDB('aio')

for con_id in con_id_list:
    con_id = int(con_id)
    generator = SOAGenerator(con_id, db_con)

db_con.close()